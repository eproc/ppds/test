## **Welcome to the Public Procurement Data Space (PPDS) Workshops Gitlab!**


The European Commission is introducing the Public Procurement Data Space (PPDS), which will be the first EU-wide platform for accessing public procurement data scattered across EU, national, and regional levels. The PPDS will improve data quality, availability and completeness by using the new eForms that allow public buyers to provide information in a more structured way. The wealth of data from PPDS will be combined with advanced analytics technologies, including Artificial Intelligence (AI), facilitating the access to public procurement data, providing better value for money for public buyers, and promoting a green, social and innovative economy.

The aim of this repository is to display the materials used during the PPDS workshops, including the co-creation sessions, and any other discussions.





[Get in touch with us!](mailto:EC-PPDS@ec.europa.eu?subject=PPDS - contact){ .md-button }
