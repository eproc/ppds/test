# PPDS Co-creation session 27 June 2024 - PPDS Live guided tour

## Agenda  

* **10:00 - 10:15**: PPDS State-of-play
* **10:15 - 12:00**: PPDS in practice
    1. Monitoring of Public Procurement Market- focus on Single Bidder
    2. Data Quality measurement
    3. Strategic Procurement measurement
    4. **11:25 – 11:40**: _Coffee break_
    5. Open discussion: Indicators and Business Rules structure

* **12:00 - 12:20**: The PPDS Day- The Grand Opening
* **12:15 - 12:30**: Wrap-up & QAs

## Session Rules

1. During the presentation keep your **mic off**.
2. Use the **chat** to ask your questions.
3. Do not be shy to **participate actively** throughout the session.
4. Please **raise your hand** if you wish to take the floor.

## 1. PPDS State-of-play

Welcome to the **PPDS Live guided tour**!
We will be focusing on the latest advancements regarding the PPDS. During the last few months, the PPDS has made significant progress in developing the data space that will:

- Enable access to public procurement data to all user groups
- Improve the monitoring of public procurement in the EU
- Reinforce transparency
- Support improving data quality
- Raise the awareness of the level of competition in public procurement
- Support cooperation between Member States and Commission
- Be open source, allowing Member States to re-use the solution

During the session we have foreseen co-creation exercises (using MIRO and Mentimeter). 
MIRO is an online collaborative tool for brainstorming and capturing ideas digitally. Should you wish to get familiar with it, we invite you to consult to a **short tutorial video here**: [Miro tutorial](https://miro.com/miroverse/miro-basics-guide-for-new-participants/).

## 2. PPDS in practice
In this section, we will address four key points related to the PPDS:

* **Monitoring of Public Procurement Market: Focus on Single Bidder:** In this segment, we will explore the public procurement market with a particular focus on single-bidder situations. Using the PPDS, we will demonstrate functionalities, data visualization, and analytical tools that help identify and assess single-bidder cases. 

* **Data Quality Measurement:** Here, we will delve into the mechanisms for measuring data quality within the PPDS application. Participants will be guided through the application’s features that assess and ensure the accuracy, completeness, and reliability of public procurement data. 

* **Strategic Procurement Measurement:** This section focuses on the strategic aspects of procurement measurement. We will showcase how the PPDS application supports strategic procurement initiatives through various analytical tools and visualization features. 

* **Open Discussion- Indicators and Business Rules Structure:** The final point will be an open discussion aimed at obtaining feedback on the structure and presentation of indicators and business rules within the PPDS application. This interactive session will involve participants in evaluating how well the indicators and business rules are displayed, seeking suggestions for improvements.

Throughout the session, we will conduct various exercises using the MIRO tool to facilitate interactive learning and feedback collection. The **PPDS Live guided tour Miro Board** has been created to facilitate the discussion and for you to add/suggest your comments. 
**Access the PPDS Data Quality Miro Board**: [PPDS Miro Board](https://miro.com/app/board/uXjVK7l0ck8=/)

## 3. The PPDS Day: The Grand Opening
📢 We are very pleased to announce that the PPDS Day – The Grand Opening, will take place on the 24th of September 2024!
This full-day event will be the occasion to officially announce - and celebrate - the implementation of the PPDS. For more information about the agenda and registration, you can consult [DG GROW Events webpage](https://single-market-economy.ec.europa.eu/events/ppds-day-2024-grand-opening-2024-09-24_en).

[Register now](https://ec.europa.eu/eusurvey/runner/PPDS-Day-Registration), we will be happy to welcome you at our event!

## 4. Outcomes of the session
**Meeting Coordinators:** AROSA Daniel/Marc christopher SCHMIDT/Antonio Soeiro

After an initial poll, here is some information about the session participants:

We had a total of **64 participants** and 9 different **types of users**:

- Policymaker
- Buyer
- SME
- Company
- Service provider (eProcurement)
- Auditor/Supervisor
- NGO
- Academia
- Other (i.e., eSenders, Data Scientists, System Developers, Policy Analysts)

**ACTIVITY 1:** Structure of the PPDS GitLab content

| SECTION | FEEDBACK |
| ------ | ------ |
| Structure| <dd>- Break down the different categories of strategic procurement. <dd>- Overview of the kind of public procurement procedure adopted, by CA, sector (non competitive vs. competitive procedures) and also by contract value.  <dd>- Strategic procurement broken down by GPP, innovation, social.  <dd>- The proposed structure looks accurate. <dd>|
|Content | <dd>- Efficiency should also involve the completion notice: how much the contract was at award time /how much it costed; and time forecast/&time of completion. <dd>- Include a Group of Procurement Techniques with indicators on FAs, DPS, eAuction, etc.  <dd>- As previously mentioned: for FAs and DPS the indicators would need to be adjusted / created differently. Needs to pondered separately.  <dd>- Regarding SME participation, include an indicator on sub-contracting + lots. <dd>- Include framework contracts and dynamic procurement problems for statistics making. <dd>- At this moment, it is unclear whether the procurement is performed by a CPB on behalf of other CAs (aggregation of procurement), potentially biasing the indicator about joint procurement. <dd> - Use of auctions in the competition group.  <dd> - For compliance, the share of total expenditures tendered out in different CPV codes over a four-year period compared to other countries is extremely low, which could indicate lower compliance. <dd>- It is important to be able to filter DPS from other techniques and to link specific procurements to the associated DPS. <dd>- Include an indicator regarding the average number of offers per procedure. <dd>- Include the following indicator for Competition: Number of times a certain supplier wins the call. <dd>- Include the following indicator for Competition: Number of times a certain supplier wins the call of a specific buyer. <dd>-Include an indicator regarding competition: Number or average of not awarded procedures. <dd>- Include the following indicator for Competition: Number of contracts by suppliers by country. <dd>- Include the following indicator: Non-award/Annulments as a measure for potential higher transaction costs. <dd>- For Strategic Procurement, include top 10 best practices to benchmark. <dd>- For Strategic Procurement, include an identification of who is following the ISO 20400 management system. <dd>- Consider sectors like health, defence, and education. |

**ACTIVITY 2:** The PPDS Day: They Grand Opening

| SECTION | FEEDBACK |
| ------ | ------ |
| Using the PPDS| <dd>- Invite / include a presentation from a MEDIA representative on what they are doing with procurement data and what other data they would need. This could be very interesting view point. <dd>- National benefit vs Pan-European Benefit. <dd>- More information regarding buyer companion.  <dd>- How can it be useful for municipalities? <dd>- Will the content of this session not be covered in the first panel? Will there be overlap with the live demo? <dd>- Limits of PPDS - it is voluntary, MS can choose which data to make available and share - how will this impact data integrity or interpretation. <dd>- Why the sessions of the PPDS launch event will not be streamed? They would be a great presentation for possible stakeholders. <dd>- How can EU use this API to know the countries that need help to leverage in strategic procurement. <dd>- Examples of practical use. <dd>- Support for End-Users: Training Materials, Videos, Support Desk. <dd>- Are there any restrictions on use? <dd>- Within-country comparisions. |
|Data Quality and the PPDS | <dd>- The mapping of the different national data sets (a graphical explanation). <dd>- Comparison of national data sets gathered in the PPDS (shared data and national specific data). On national specific data the strategy to make good use of them.  <dd>- The impact of not making the data available correctly (practical examples).  <dd>- Are e-Forms validation rules sufficient? <dd>- Using the same terminology used in the eForms.  <dd>- Cross-validation between national data and PPDS. |
|The technical side of the PPDS | <dd>- How can we manage/eliminate incorrect data from indicators? <dd>- To implement the PPDS: type of resources needed, minimum duration of the project, funding information.  <dd>- Implementation roadmap. <dd>- Outliers, data validation and quality improvement. <dd> |