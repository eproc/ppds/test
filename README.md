# Readme

## Name

GitLab Space for the Public Procurement Data Space (PPDS) Workshops

## Description

This GitLab Space is used for all workshops around the PPDS. Each workshop is separated through a folder where you can find all relevant content.

## Contributing

If you would like to participate in workshops around the PPDS, please write to <ec-ppds@ec.europa.eu>. You have also the possibility to provide an issue on https://code.europa.eu/eproc/ppds/crs/-/issues. For this, you need to log in with your EU Login.

## Background

Every year in the EU, over 250 000 public authorities spend around EUR 2 trillion (around 13,6 % of GDP) on the purchase of services, works and supplies. From new roads to tablets for pupils or equipment for hospitals: Public authorities are the main buyers in many key sectors such as energy, transport, infrastructure, waste management, social protection, health, defence and education services.

By making the right purchasing choices, the public sector can boost jobs, growth, and investment in Europe and create a more innovative, competitive, energy-efficient, and socially inclusive economy. This can support key policies at national and EU level, such the recovery from the pandemic, the twin transitions, the strategic autonomy of the EU and its digital leadership.

To unlock the full potential of public procurement, access to data and the ability to analyse it are essential. However, data from only 20 % of all call for tenders as submitted by public buyers is available and searchable for analysis in one place. The remaining 80 % are spread, in different formats, at national or regional level and difficult or impossible to re-use for policy, transparency and better spending purposes. In order words, public procurement is rich in data, but poor in making it work for taxpayers, policy makers and public buyers.

The initiative on the Public Procurement Data Space (PPDS) aims to harness the power of data available throughout the EU.
